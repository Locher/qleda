from pathlib import Path

# Testing
DIR_TEST_OUT = 'tdat_out'
DIR_TEST_IN = 'tdat_in'

DIR_TEST_DBS = 'dbs'

DIR_DATA = 'data'
DIR_JSON_DUMPS = 'json_dumps'
DIR_LIBRARIES = 'libraries'
DIR_SCHEMATIC = 'schematic'


