*PADS-LIBRARY-SCH-DECALS-V9*

$OSR_BI_LEFT     33500 33180 97 10 97 10 2 1 0 1 24
TIMESTAMP 2015.02.20.18.38.43
"Default Font"
"Default Font"
4     16    0 0 83 5 "Default Font"
REF-DES
-190  0     0 9 83 5 "Default Font"
PART-TYPE
OPEN   8 10 0 -1
0     0    
-20   0    
-60   40   
-120  40   
-160  0    
-120  -40  
-60   -40  
-20   0    
T0     0     0 0 0     0     0 0 0     0     0 0 
P0     0     0 0 0     0     0 0 0

$OSR_BI_RIGHT    33500 33180 100 10 100 10 2 1 0 1 24
TIMESTAMP 2015.02.20.18.38.51
"Default Font"
"Default Font"
0     16    0 1 80 5 "Default Font"
REF-DES
194   0     0 8 80 5 "Default Font"
PART-TYPE
OPEN   8 10 0 -1
0     0    
20    0    
60    40   
120   40   
160   0    
120   -40  
60    -40  
20    0    
T0     0     0 0 0     0     0 0 0     0     0 0 
P0     0     0 0 0     0     0 0 0

GNDC             32000 32000 100 10 100 10 2 7 0 1 30
TIMESTAMP 2015.02.22.16.30.18
"Default Font"
"Default Font"
60    -60   0 0 100 10 "Default Font"
REF-DES
0     200   0 0 100 10 "Default Font"
PART-TYPE
OPEN   2 10 0 -1
0     0    
0     -100 
OPEN   2 10 0 -1
-100  -100 
100   -100 
OPEN   2 10 0 -1
0     -100 
-100  -200 
OPEN   2 10 0 -1
100   -100 
0     -200 
OPEN   2 10 0 -1
50    -100 
-50   -200 
OPEN   2 10 0 -1
50    -200 
100   -150 
OPEN   2 10 0 -1
-50   -100 
-100  -150 
T0     0     0 0 0     0     0 0 0     0     0 16 
P0     0     0 0 0     0     0 0 0

GNDE             32000 32000 100 10 100 10 2 4 0 1 30
TIMESTAMP 2015.02.22.16.30.13
"Default Font"
"Default Font"
60    -60   0 0 100 10 "Default Font"
REF-DES
0     200   0 0 100 10 "Default Font"
PART-TYPE
OPEN   2 10 0 -1
0     0    
0     -100 
OPEN   2 10 0 -1
-100  -100 
100   -100 
OPEN   2 10 0 -1
-60   -140 
60    -140 
OPEN   2 10 0 -1
-20   -180 
20    -180 
T0     0     0 0 0     0     0 0 0     0     0 16 
P0     0     0 0 0     0     0 0 0

GNDT             32000 32000 100 10 100 10 2 2 0 1 30
TIMESTAMP 2015.02.22.16.30.03
"Default Font"
"Default Font"
0     -210  0 4 100 10 "Default Font"
REF-DES
0     200   0 0 100 10 "Default Font"
PART-TYPE
OPEN   2 10 0 -1
0     0    
0     -100 
OPEN   2 10 0 -1
-50   -100 
50    -100 
T0     0     0 0 0     0     0 0 0     0     0 16 
P0     0     0 0 0     0     0 0 0

N318DRES         33100 32348 0 0 0 0 4 13 0 3 2
TIMESTAMP 1999.08.25.21.39.16
"Default Font"
"Default Font"
-230  460   0 0 100 10 "Default Font"
REF-DES
570   590   0 0 100 10 "Default Font"
PART-TYPE
-230  340   0 0 100 10 "Default Font"
*
550   530   0 2 100 10 "Default Font"
*
OPEN   2 10 0 -1
300   500  
300   400  
OPEN   2 10 0 -1
350   500  
500   700  
OPEN   2 25 0 -1
200   500  
400   500  
COPCLS 4 10 0 -1
434   560  
390   602  
444   624  
434   560  
OPEN   2 10 0 -1
250   500  
100   700  
CIRCLE 2 20 0 -1
300   400  
300   832  
OPEN   10 10 0 -1
400   300  
450   300  
464   332  
488   260  
512   332  
536   260  
560   332  
584   260  
600   300  
650   300  
OPEN   10 10 0 -1
300   0    
300   50   
268   64   
340   88   
268   112  
340   136  
268   160  
340   184  
300   200  
300   250  
OPEN   2 10 0 -1
300   400  
300   250  
OPEN   2 10 0 -1
300   300  
400   300  
OPEN   2 10 0 -1
300   0    
100   0    
OPEN   2 10 0 -1
500   700  
800   700  
OPEN   3 10 0 -1
700   700  
700   300  
650   300  
T900   700   0 2 80    16    0 2 170   -30   0 16 PINSHORT
P-30   -110  0 2 0     0     0 0 128
T0     0     90 4 140   -120  90 0 -20   -40   0 18 PINVRTS
P30    900   90 2 0     0     0 0 128
T0     700   0 0 70    16    0 2 170   -30   0 16 PINSHORT
P30    -120  0 2 0     0     0 0 128

PIN              34000 34000 0 0 0 0 4 1 0 0 0
TIMESTAMP 1999.08.25.21.39.16
"Default Font"
"Default Font"
140   20    0 1 100 10 "Default Font"
REF-DES
230   0     0 8 100 10 "Default Font"
PART-TYPE
-520  0     0 1 100 10 "Default Font"
*
-80   0     0 1 100 10 "Default Font"
*
OPEN   2 10 0 -1
0     0
200   0

PINSHORT         34000 34000 0 0 0 0 4 1 0 0 0
TIMESTAMP 1999.08.25.21.39.16
"Default Font"
"Default Font"
60    10    0 1 100 10 "Default Font"
REF-DES
140   10    0 8 100 10 "Default Font"
PART-TYPE
-530  10    0 1 100 10 "Default Font"
*
-70   10    0 1 100 10 "Default Font"
*
OPEN   2 10 0 -1
0     0
100   0

PINVRTS          34000 34000 0 0 0 0 4 1 0 0 3
TIMESTAMP 1999.08.25.21.39.16
"Default Font"
"Default Font"
20    -80   0 0 100 10 "Default Font"
REF-DES
-20   -40   0 9 100 10 "Default Font"
PART-TYPE
-500  10    0 1 100 10 "Default Font"
*
-30   14    0 1 100 10 "Default Font"
*
OPEN   2 10 0 -1
0     -100
0     0

RESVAR-H1        32750 32774 0 0 0 0 4 2 0 3 0
TIMESTAMP 1999.08.25.21.39.16
"Default Font"
"Default Font"
250   450   0 4 100 10 "Default Font"
REF-DES
250   400   0 6 100 10 "Default Font"
PART-TYPE
250   300   0 6 100 10 "Default Font"
*
250   200   0 6 100 10 "Default Font"
*
COPCLS 4 10 0 -1
250   -60  
270   -130 
230   -130 
250   -60  
OPEN   8 10 0 -1
100   0    
130   -60  
190   60   
250   -60  
310   60   
360   -60  
420   60   
450   0    
T550   0     0 2 60    -120  0 2 170   -30   0 16 PINSHORT
P-144  -36   0 2 0     0     0 0 128
T0     0     0 4 60    50    0 2 170   -30   0 16 PINSHORT
P-150  -32   0 2 0     0     0 0 128
T250   -200  90 2 80    130   90 2 170   -30   0 16 PINSHORT
P-176  456   90 2 0     0     0 0 128

RESVAR-H2        33200 32674 0 0 0 0 4 2 0 3 0
TIMESTAMP 1999.08.25.21.39.16
"Default Font"
"Default Font"
350   -200  0 4 100 10 "Default Font"
REF-DES
350   -250  0 6 100 10 "Default Font"
PART-TYPE
350   -350  0 6 100 10 "Default Font"
*
350   -450  0 6 100 10 "Default Font"
*
OPEN   8 10 0 -1
200   0    
230   60   
290   -60  
350   60   
410   -60  
460   60   
520   -60  
550   0    
COPCLS 4 10 0 -1
350   60   
330   130  
370   130  
350   60   
T750   0     0 2 140   20    0 2 230   0     0 16 PIN
P-150  0     0 2 0     0     0 0 128
T0     0     0 0 140   20    0 2 230   0     0 16 PIN
P-300  0     0 2 0     0     0 0 128
T350   300   90 4 136   86    90 2 230   0     0 16 PIN
P-100  400   90 2 0     0     0 0 128

RESVAR-V1        33500 32474 0 0 0 0 4 2 0 3 0
TIMESTAMP 1999.08.25.21.39.16
"Default Font"
"Default Font"
-450  450   0 4 100 10 "Default Font"
REF-DES
-450  400   0 6 100 10 "Default Font"
PART-TYPE
-450  250   0 6 100 10 "Default Font"
*
-450  150   0 6 100 10 "Default Font"
*
COPCLS 4 10 0 -1
60    300  
130   320  
130   280  
60    300  
OPEN   8 10 0 -1
0     450  
60    420  
-60   360  
60    300  
-60   240  
60    190  
-60   130  
0     100  
T0     0     90 0 112   96    90 2 170   -30   0 16 PINSHORT
P82    -68   90 2 0     0     0 0 128
T0     550   90 4 100   96    90 2 170   -30   0 16 PINSHORT
P78    -50   90 2 0     0     0 0 128
T300   300   0 2 140   20    0 2 230   0     0 16 PIN
P-500  10    0 2 0     0     0 0 128

RESVAR-V2        33500 32424 0 0 0 0 4 2 0 3 0
TIMESTAMP 1999.08.25.21.39.16
"Default Font"
"Default Font"
400   300   0 4 100 10 "Default Font"
REF-DES
400   250   0 6 100 10 "Default Font"
PART-TYPE
400   50    0 6 100 10 "Default Font"
*
400   150   0 6 100 10 "Default Font"
*
OPEN   8 10 0 -1
0     450  
-60   420  
60    360  
-60   300  
60    240  
-60   190  
60    130  
0     100  
COPCLS 4 10 0 -1
-60   300  
-130  320  
-130  280  
-60   300  
T0     0     90 0 110   110   90 2 170   -30   0 16 PINSHORT
P-300  400   90 2 0     0     0 0 128
T0     550   90 4 90    110   90 2 170   -30   0 16 PINSHORT
P-150  400   90 2 0     0     0 0 128
T-200  300   0 4 50    50    0 2 170   -30   0 16 PINSHORT
P-200  -50   0 2 0     0     0 0 128

VCC              32000 32000 97 10 97 10 2 2 0 1 30
TIMESTAMP 2015.02.20.18.43.15
"Default Font"
"Default Font"
0     240   0 4 97 10 "Default Font"
REF-DES
0     200   0 0 97 10 "Default Font"
PART-TYPE
OPEN   4 10 0 -1
0     220  
-40   100  
40    100  
0     220  
OPEN   2 10 0 -1
0     100  
0     0    
T0     0     0 6 0     0     0 0 0     0     0 0 
P350   0     0 0 0     0     0 0 0

VCC-             32000 32000 97 10 97 10 2 1 0 1 30
TIMESTAMP 2015.02.20.18.39.29
"Default Font"
"Default Font"
0     -310  0 4 97 10 "Default Font"
REF-DES
0     200   0 0 97 10 "Default Font"
PART-TYPE
OPEN   6 10 0 -1
0     0    
0     -100 
40    -100 
0     -220 
-40   -100 
0     -100 
T0     0     0 6 0     0     0 0 0     0     0 0 
P0     0     0 0 0     0     0 0 0

*END*
