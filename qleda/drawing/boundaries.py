from typing import NamedTuple


class Boundaries(NamedTuple):
    min_x: float
    min_y: float
    max_x: float
    max_y: float