from ..drawingpieces import Rect, Point, Line, Text, Circle, Arc
from enum import IntEnum


_mapping_python_sqlalchemy = {
    str: 'String',
    float: 'Float',
    bool: 'Boolean',
    int: 'Integer'
}


def _build_attr_name(field: str, prefixes: list):
    return '_'.join([*[p.lower() for p in prefixes], field])


def _build_composite_line(field_name, cls):
    gen_from_row = '._generate_from_row' if True in [hasattr(c, '_nested_structure')
                                                    for c in cls._field_types.values()] else ''
    return f'{field_name} = composite({cls.__name__}{gen_from_row}'


def _iter_atrs(d_iter: dict, prefixes: list,  lines_normal: list, lines_composite: list):
    normal_attrs = []
    for field, t in d_iter.items():
        field_name = _build_attr_name(field, prefixes)
        if hasattr(t, '_nested_structure'):
            comp_line = _build_composite_line(field_name, t)
            new_prefixes = [*prefixes, field]
            subpart_has_normal = True if False in [hasattr(v, '_nested_structure') for v in t._field_types.values()]\
                else False

            if subpart_has_normal:
                lines_normal.extend(['', '# {}'.format(' '.join(new_prefixes))])
            subpart_normal_attrs = _iter_atrs(t._field_types, new_prefixes, lines_normal, lines_composite)
            if subpart_has_normal:
                lines_normal.append('')

            comp_line += ', {})'.format(', '.join(subpart_normal_attrs))
            lines_composite.append(comp_line)

            normal_attrs.extend(subpart_normal_attrs)
        else:
            normal_attrs.append(field_name)
            if issubclass(t, IntEnum):
                lines_normal.append(f'{field_name} = Column(Integer, nullable=False)')
            else:
                lines_normal.append(f'{field_name} = Column({_mapping_python_sqlalchemy[t]}, nullable=False)')

    return normal_attrs


def _print_DbClass(cls):
    if not hasattr(cls, '_nested_structure'):
        raise TypeError
    name = cls.__name__
    name_l = name.lower()

    normal_lines = []
    composite_lines = []
    normal_attrs = _iter_atrs(cls._field_types, [], normal_lines, composite_lines)
    comp_line = _build_composite_line(cls.__name__.lower(), cls)
    comp_line += ', {})'.format(', '.join(normal_attrs))

    composite_lines.append(comp_line)


    lines = [
        f'class Db{name}(DbDrawingPiece):',
        '"""',
        f'Db class of {name}. Attributes definition done by _print_DbClass',
        '"""',
        f'__tablename__ = "{name_l}"',
        '',
        'id = Column(Integer, ForeignKey("dp.id"), primary_key=True)',
        '',
        *normal_lines,
        '',
        '# composite attributes',
        *composite_lines,
        '',
        '__mapper_args__ = {',
        f"    'polymorphic_identity': '{name_l}',",
        '}',
        '',
        'def __repr__(self):',
        f'    return "Db{name}(id={{}}, {name_l}={{}})".format(self.id, self.{name_l})'

    ]
    print('\n'.join([lines[0], *['    ' + line for line in lines[1:]]]))


if __name__ == '__main__':

    for drawing_piece in [Point, Rect, Line, Text, Circle, Arc]:
        _print_DbClass(drawing_piece)
        print('\n')