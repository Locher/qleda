.. qleda documentation master file, created by
   sphinx-quickstart on Thu Jan 21 21:16:21 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to qleda's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   model

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
